Django==1.8.1
argparse==1.2.1
djangorestframework==3.1.1
ipdb==0.8.1
ipython==3.1.0
wsgiref==0.1.2
