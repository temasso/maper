from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.clickjacking import xframe_options_exempt

from .models import Point
from .forms import PointUploadForm


def index(request):
    template = 'main.html'

    return render(request, template, {'STATIC_URL': settings.STATIC_URL})

@csrf_exempt
def upload(request):
    """longitude, latitude, media, text, type"""
    print(request)

    if request.method == 'POST':
        form = PointUploadForm(request.POST, request.FILES)
        if form.is_valid():
            lon = form.cleaned_data['longitude']
            lat = form.cleaned_data['latitude']
            text = form.cleaned_data['text']
            media = form.cleaned_data['media']
            mtype = form.cleaned_data['type']
            point = Point(longitude=lon, latitude=lat, text=text, media=media, type=mtype)
            point.save()
            return HttpResponse(point.pid, status=201)
        else:
            return HttpResponse(str(form.errors))

    else:
        return HttpResponse('Request should be of type POST')