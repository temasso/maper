import random


ID_ALPHABET = '23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'
ID_LENGTH = 8


def generate_id():
    sr = random.SystemRandom()
    return ''.join(sr.choice(ID_ALPHABET) for i in range(ID_LENGTH))
