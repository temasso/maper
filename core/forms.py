from django import forms


class PointUploadForm(forms.Form):

    TEXT = 'text'
    IMAGE = 'image'
    AUDIO = 'audio'
    VIDEO = 'video'
    POINT_TYPES = (
        (TEXT, TEXT),
        (IMAGE, IMAGE),
        (AUDIO, AUDIO),
        (VIDEO, VIDEO)
    )

    longitude = forms.FloatField()
    latitude = forms.FloatField()

    media = forms.FileField(allow_empty_file=True, required=False)
    text = forms.CharField(max_length=1000)

    type = forms.ChoiceField(choices=POINT_TYPES)





