from django.conf.urls import url, include
from .views import index, upload

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^upload/', upload, name='upload'),
]
