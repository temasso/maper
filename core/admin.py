from django.contrib import admin
from .models import Point, Rating

@admin.register(Point)
class PointAdmin(admin.ModelAdmin):
    pass

@admin.register(Rating)
class RatingAdmin(admin.ModelAdmin):
    pass