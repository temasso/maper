from rest_framework import serializers
from .models import Point


class PointSerializer(serializers.ModelSerializer):
    icon_url = serializers.ReadOnlyField()
    id = serializers.ReadOnlyField(source="pid")

    class Meta:
        model = Point