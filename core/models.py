from django.db import models
from .utils import ID_LENGTH, generate_id

# Create your models here.


class Point(models.Model):
    TEXT = 'text'
    IMAGE = 'image'
    AUDIO = 'audio'
    VIDEO = 'video'
    POINT_TYPES = (
        (TEXT, TEXT),
        (IMAGE, IMAGE),
        (AUDIO, AUDIO),
        (VIDEO, VIDEO)
    )

    pid = models.CharField(primary_key=True, max_length=ID_LENGTH, blank=True)
    text = models.TextField()
    add_date = models.DateTimeField(auto_now_add=True)

    longitude = models.FloatField()
    latitude = models.FloatField()

    type = models.CharField(choices=POINT_TYPES, max_length=6)
    media = models.FileField(null=True, blank=True)

    @property
    def icon_url(self):
        return "/static/img/{}.png".format(self.type)

    def rate_average(self):
        from django.db.models import Sum
        rates = self.rating.all()
        sums = rates.aggregate(Sum('value'))
        get_sums = sums.get('value__sum', 0)
        quantity = rates.count()
        return get_sums / quantity

    def __unicode__(self):
        return self.text

    class Meta:
        ordering = ['-add_date', ]

    def save(self, *args, **kwargs):
        if self.pk == '':
            points = Point.objects.filter(latitude__iexact=self.latitude, longitude__iexact=self.longitude)
            if points.count():
                self.latitude += 0.01  # przesun nieco aby punkty sie nie nakladaly

        self.pk = generate_id()
        super(Point, self).save(*args, **kwargs)


class Rating(models.Model):
    point = models.ForeignKey(Point, related_name='rating')
    value = models.SmallIntegerField()
