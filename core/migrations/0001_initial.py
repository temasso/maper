# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.utils


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Point',
            fields=[
                ('pid', models.CharField(default=core.utils.generate_id, max_length=8, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=150)),
                ('add_date', models.DateTimeField(auto_now_add=True)),
                ('longitude', models.FloatField()),
                ('latitude', models.FloatField()),
                ('type', models.CharField(max_length=6, choices=[(b'text', b'text'), (b'image', b'image'), (b'audio', b'audio'), (b'video', b'video')])),
                ('media', models.FileField(null=True, upload_to=b'')),
            ],
        ),
    ]
