# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20150606_0936'),
    ]

    operations = [
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.SmallIntegerField()),
            ],
        ),
        migrations.AlterField(
            model_name='point',
            name='pid',
            field=models.CharField(max_length=8, serialize=False, primary_key=True, blank=True),
        ),
        migrations.AddField(
            model_name='rating',
            name='point',
            field=models.ForeignKey(related_name='rating', to='core.Point'),
        ),
    ]
