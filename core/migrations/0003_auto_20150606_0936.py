# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20150604_2009'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='point',
            options={'ordering': ['-add_date']},
        ),
        migrations.RenameField(
            model_name='point',
            old_name='title',
            new_name='text',
        ),
    ]
