var app = angular.module('MaperApp', ['ngResource', 'ui.bootstrap', 'uiGmapgoogle-maps', 'yaru22.angular-timeago'])

.config(function($httpProvider) {
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    })

.config(function($locationProvider){
        $locationProvider.html5Mode(false);
    })

.run(function($rootScope) {
        $rootScope.STATIC_URL = window.STATIC_URL;
    })

.provider('MarkersResource', {
    $get: function($q, $resource) {
        return $resource('/api/points/:pid/', {week_id: '@pid'});
    }
});

app.controller('MapController', function($scope, $modal, MarkersResource, uiGmapGoogleMapApi) {
    $scope.points = MarkersResource.query();
    $scope.map = { center: { latitude: 52.221372, longitude: 21.007121 }, zoom: 8, bounds: {},
        //events: {
         //   tilesloaded: function (map) {
         //       $scope.$apply(function () {
         //           //$scope.mapInstance = map;
         //       });
         //   }
        //},
        markersEvents: {
            click: function(marker, eventName, model, args) {
                $scope.map.center = { latitude: marker.model.latitude, longitude: marker.model.longitude };
                $scope.map.zoom = 10;
                var point = undefined;
                for (var i = 0; i < $scope.points.length; i++) {
                    if ($scope.points[i].id == model.id) {
                        point = $scope.points[i];
                        break;
                    }
                }
                if (point) {
                    var modal = $modal.open({
                      templateUrl: window.STATIC_URL + 'partials/mapper/modal.html',
                      controller: 'PointModalController',
                      resolve: {
                        params: function () {
                          return {
                              point: point
                          }
                        }
                      }
                    });
                }
            }
        }
    };
    $scope.icons = {
        text: $scope.STATIC_URL + 'img/text.png',
        video: $scope.STATIC_URL + 'img/video.png',
        image: $scope.STATIC_URL + 'img/image.png',
        audio: $scope.STATIC_URL + 'img/audio.png'
    };
    $scope.marki = [];

    $scope.points.$promise.then(function(points){
        angular.forEach(points, function (point, key) {
            var marker = {
                id: point.pid,
                latitude: point.latitude,
                longitude: point.longitude,
                title: point.text
            };
            if(point.type == 'text'){
                marker['icon'] = $scope.icons.text;
            } else if (point.type == 'video') {
                marker['icon'] = $scope.icons.video;
            } else if (point.type == 'image') {
                marker['icon'] = $scope.icons.image;
            }  else if (point.type == 'audio') {
                marker['icon'] = $scope.icons.audio;
            }
            $scope.marki.push(marker);
            $scope.map.center = { latitude: $scope.marki[0].latitude, longitude: $scope.marki[0].longitude };
            $scope.map.zoom = 4;
        });
    });

    uiGmapGoogleMapApi.then(function(maps) {
        $scope.mapInstance = maps;
    });

    $scope.centerFromMessage = function(point){
        $scope.map.center = { latitude: point.latitude, longitude: point.longitude };
        $scope.map.zoom = 14;
        console.log($scope.mapInstance);
        //point.setAnimation($scope.mapInstance.Animation.BOUNCE);
        //console.log($scope.marki);
    }
})

.directive('markerItem', function() {
    return {
        restrict: 'EA',
        scope: {
          marker: '=markerItem',
          show_text: '=showText'
        },
        templateUrl: window.STATIC_URL + 'partials/mapper/item.html',
        link: function(scope, element, attrs) {

        }
    };
})


.controller('PointModalController', function($scope, params) {
    $scope.point = params.point;
});

